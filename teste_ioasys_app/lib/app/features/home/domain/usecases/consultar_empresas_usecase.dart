import 'package:dartz/dartz.dart';
import 'package:teste_ioasys_app/app/core/network/api_result.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';
import 'package:teste_ioasys_app/app/features/home/domain/repositories/i_empresas_repository.dart';
import 'package:teste_ioasys_app/injection_container.dart';

class ConsultarEmpresasUsecase {
  ConsultarEmpresasUsecase({
    IEmpresasRepository repository,
  }) : _repository = repository ?? dependency<IEmpresasRepository>();

  final IEmpresasRepository _repository;

  Future<Either<Erro, List<Company>>> call({String consultName}) async {
    return _repository.getCompanies(consultName: consultName);
  }
}
