import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:teste_ioasys_app/app/core/network/api_result.dart';
import 'package:teste_ioasys_app/app/features/home/data/datasources/empresas_datasource.dart';
import 'package:teste_ioasys_app/app/features/home/data/models/empresa_model.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';
import 'package:teste_ioasys_app/app/features/home/domain/repositories/i_empresas_repository.dart';
import 'package:teste_ioasys_app/injection_container.dart';

class CompaniesRepository implements IEmpresasRepository {
  CompaniesRepository({
    @required CompaniesDatasource companiesDatasource,
  }) : _companiesDatasource =
            companiesDatasource ?? dependency<CompaniesDatasource>();

  CompaniesDatasource _companiesDatasource;

  @override
  Future<Either<Erro, List<Company>>> getCompanies({String consultName}) async {
    final result =
        await _companiesDatasource.getCompanies(consultName: consultName);

    if (result is Success) {
      final companies = (result.data['enterprises'] as List)
          .map((dynamic json) => CompanyModel.fromJson(json))
          .toList();

      return Right(companies);
    } else {
      return Left(result);
    }
  }
}
