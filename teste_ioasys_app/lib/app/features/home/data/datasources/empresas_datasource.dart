import 'package:teste_ioasys_app/app/core/network/api_result.dart';
import 'package:teste_ioasys_app/app/core/network/http_methods.dart';
import 'package:teste_ioasys_app/app/core/network/request_handler.dart';

class CompaniesDatasource {
  Future<ApiResult> getCompanies({String consultName}) async {
    return await RequestHandler.request(
      httpMethod: Get(
        url: 'enterprises${consultName != null ? '?name=$consultName' : ''}',
      ),
    );
  }
}
