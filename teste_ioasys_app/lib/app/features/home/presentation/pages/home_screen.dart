import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:teste_ioasys_app/app/common/coordinator/main_coordinator.dart';
import 'package:teste_ioasys_app/app/common/ui/carregamento_widget.dart';
import 'package:teste_ioasys_app/app/common/ui/cores.dart';
import 'package:teste_ioasys_app/app/common/ui/strings.dart';
import 'package:teste_ioasys_app/app/features/home/presentation/components/cabecalho_retangulo_widget.dart';
import 'package:teste_ioasys_app/app/features/home/presentation/components/campo_pesquisa_texto_home.dart';
import 'package:teste_ioasys_app/app/features/home/presentation/components/lista_empresas_widget.dart';
import 'package:teste_ioasys_app/app/features/home/presentation/cubit/home_cubit.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<HomeCubit>(
        create: (context) => HomeCubit()..consultCompany(),
        child: BlocConsumer<HomeCubit, HomeState>(
          listener: (context, state) {
            if (state is HomeFailedCredentials) {
              MainCoordinator.navigateToLogin();
            }
          },
          builder: (context, state) {
            if (state is HomeLoading) {
              return Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      HeaderRetangleWidget(),
                      _widgetCenterScreen(
                        child: LoadingWidget(),
                      ),
                    ],
                  ),
                  FieldSearchTextWidget(),
                ],
              );
            }

            if (state is HomeLoaded) {
              return Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      HeaderRetangleWidget(),
                      state.companies.length > 0
                          ? ListCompaniesWidget(companies: state.companies)
                          : _widgetCenterScreen(
                              child: Text(
                                Strings.noResult,
                                style: GoogleFonts.rubik(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w300,
                                    color: Cores.darkGray),
                              ),
                            ),
                    ],
                  ),
                  FieldSearchTextWidget(),
                ],
              );
            }

            if (state is HomeError) {
              return Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      HeaderRetangleWidget(),
                      _widgetCenterScreen(
                        child: Text(
                          state.mensageError,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.rubik(
                              fontSize: 18,
                              fontWeight: FontWeight.w300,
                              color: Cores.darkGray),
                        ),
                      )
                    ],
                  ),
                  FieldSearchTextWidget(),
                ],
              );
            }

            return Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    HeaderRetangleWidget(),
                  ],
                ),
                FieldSearchTextWidget(),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _widgetCenterScreen({Widget child}) {
    return Expanded(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: child,
        ),
      ),
    );
  }
}
