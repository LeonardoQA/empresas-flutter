part of 'home_cubit.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  const HomeInitial();

  @override
  List<Object> get props => [];
}

class HomeLoading extends HomeState {
  const HomeLoading();

  @override
  List<Object> get props => [];
}

class HomeLoaded extends HomeState {
  const HomeLoaded({this.companies});

  final List<Company> companies;

  @override
  List<Object> get props => [];
}

class HomeError extends HomeState {
  const HomeError({this.mensageError});

  final String mensageError;

  @override
  List<Object> get props => [];
}

class HomeFailedCredentials extends HomeState {
  @override
  List<Object> get props => [];
}
