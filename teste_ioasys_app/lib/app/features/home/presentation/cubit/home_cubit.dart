import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';
import 'package:teste_ioasys_app/app/features/home/domain/usecases/consultar_empresas_usecase.dart';
import 'package:teste_ioasys_app/injection_container.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit({ConsultarEmpresasUsecase consultarEmpresasUsecase})
      : _consultarEmpresasUsecase =
            consultarEmpresasUsecase ?? dependency<ConsultarEmpresasUsecase>(),
        super(HomeInitial());

  ConsultarEmpresasUsecase _consultarEmpresasUsecase;

  void consultCompany({String consultName}) async {
    emit(HomeLoading());

    final resultado = await _consultarEmpresasUsecase(consultName: consultName);

    resultado.fold(
      (erro) {
        if (erro.statusCode == 401) {
          emit(HomeFailedCredentials());
          return;
        }

        emit(HomeError(mensageError: erro.message));
      },
      (resposta) {
        emit(HomeLoaded(companies: resposta));
      },
    );
  }
}
