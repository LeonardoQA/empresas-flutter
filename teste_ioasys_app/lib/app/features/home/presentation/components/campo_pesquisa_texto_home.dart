import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teste_ioasys_app/app/common/ui/campo_entrada_texto_widget.dart';
import 'package:teste_ioasys_app/app/common/ui/strings.dart';
import 'package:teste_ioasys_app/app/common/utils/dimensionamento_utils.dart';
import 'package:teste_ioasys_app/app/features/home/presentation/cubit/home_cubit.dart';

class FieldSearchTextWidget extends StatefulWidget {
  @override
  _FieldSearchTextWidgetState createState() => _FieldSearchTextWidgetState();
}

class _FieldSearchTextWidgetState extends State<FieldSearchTextWidget>
    with WidgetsBindingObserver {
  Timer _debounce;
  String _queryAnterior = '';
  bool _isOpenKeyboard = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _debounce?.cancel();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final bottomInset = WidgetsBinding.instance.window.viewInsets.bottom;
    final newValue = bottomInset > 0.0;
    if (newValue != _isOpenKeyboard) {
      setState(() {
        _isOpenKeyboard = newValue;
      });
    }
  }

  _onSearchChanged(String query) {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (query != _queryAnterior) {
        _queryAnterior = query;
        BlocProvider.of<HomeCubit>(context)
            .consultCompany(consultName: query.toLowerCase());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 250),
      padding: EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        top: SizingUtils.heightTextHeaderRectangle(context, _isOpenKeyboard),
      ),
      child: TextInputFieldWidget(
        prefixIcon: Icons.search,
        fontSize: 18,
        opacity: 0.7,
        hintText: Strings.searchByCompany,
        onChanged: _onSearchChanged,
      ),
    );
  }
}
