import 'package:flutter/material.dart';
import 'package:teste_ioasys_app/app/common/ui/icones.dart';
import 'package:teste_ioasys_app/app/common/utils/dimensionamento_utils.dart';

class HeaderRetangleWidget extends StatefulWidget {
  @override
  _HeaderRetangleWidgetState createState() => _HeaderRetangleWidgetState();
}

class _HeaderRetangleWidgetState extends State<HeaderRetangleWidget>
    with WidgetsBindingObserver {
  bool _isOpenKeyboard = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final bottomInset = WidgetsBinding.instance.window.viewInsets.bottom;
    final newValue = bottomInset > 0.0;
    if (newValue != _isOpenKeyboard) {
      setState(() {
        _isOpenKeyboard = newValue;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: _ClipRectangle(),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 250),
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        height: SizingUtils.heightHeaderRectangle(context, _isOpenKeyboard),
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: ExactAssetImage(Icones.background),
          ),
        ),
        child: AnimatedOpacity(
          curve: _isOpenKeyboard ? Curves.easeOutCirc : Curves.easeInCirc,
          duration: Duration(milliseconds: 250),
          opacity: _isOpenKeyboard ? 0.0 : 1.0,
          child: Stack(
            children: [
              Container(
                alignment: Alignment.bottomLeft,
                child: Transform.translate(
                  offset: Offset(0.0, 20.0),
                  child: Image.asset(
                    Icones.logoIconLowOpacity1,
                    scale: 4.0,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.topRight,
                child: Transform.translate(
                  offset: Offset(0.0, 0.0),
                  child: Image.asset(
                    Icones.logoIconLowOpacity4,
                    scale: 3.5,
                  ),
                ),
              ),
              Align(
                alignment: Alignment(-0.7, 0.0),
                child: Image.asset(
                  Icones.logoIconLowOpacity3,
                  scale: 3.5,
                ),
              ),
              Transform.translate(
                offset: Offset(0.0, 20.0),
                child: Align(
                  alignment: Alignment(0.6, 1.0),
                  child: Image.asset(
                    Icones.logoIconLowOpacity2,
                    scale: 3.5,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ClipRectangle extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path()
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, 0)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
