import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:teste_ioasys_app/app/common/coordinator/main_coordinator.dart';
import 'package:teste_ioasys_app/app/common/ui/empresa_titulo_imagem_widget.dart';
import 'package:teste_ioasys_app/app/common/ui/strings.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';

class ListCompaniesWidget extends StatelessWidget {
  ListCompaniesWidget({
    @required this.companies,
  });

  final List<Company> companies;
  final int _nextPageThreshold = 5;

  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          padding: EdgeInsets.zero,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: companies.length + 1,
          itemBuilder: (context, index) {
            if (index == 0) {
              return Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 40.0, 16.0, 16.0),
                child: Text(
                  Strings.resultsFound(companies.length),
                  style: GoogleFonts.rubik(
                    fontSize: 14,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              );
            } else {
              log(companies.length.toString());
              if (index == index) {
                return Padding(
                  padding: const EdgeInsets.only(
                      bottom: 8.0, left: 16.0, right: 16.0),
                  child: EmpresaTituloImagemWidget(
                    company: companies[index - 1],
                    borderRadius: 4.0,
                    onPressed: (_) {
                      MainCoordinator.navigateToCompanyDetails(
                          companies[index - 1]);
                    },
                  ),
                );
              }
            }
            ;
          }),
    );
  }
}
