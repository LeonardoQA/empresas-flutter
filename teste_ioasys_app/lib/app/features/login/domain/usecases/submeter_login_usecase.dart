import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:dartz/dartz.dart';
import 'package:teste_ioasys_app/app/core/network/api_result.dart';
import 'package:teste_ioasys_app/injection_container.dart';
import 'package:teste_ioasys_app/app/features/login/domain/repositories/i_login_repository.dart';

class submitLoginUsecase {
  submitLoginUsecase({
    ILoginRepository repository,
  }) : _repository = repository ?? dependency<ILoginRepository>();

  final ILoginRepository _repository;

  Future<Either<Erro, Headers>> call({
    @required String email,
    @required String password,
  }) async {
    return _repository.submitLogin(
      email: email,
      password: password,
    );
  }
}
