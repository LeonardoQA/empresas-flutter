part of 'login_cubit.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class LoginInitial extends LoginState {
  const LoginInitial();

  @override
  List<Object> get props => [];
}

class LoginLoading extends LoginState {
  const LoginLoading();

  @override
  List<Object> get props => [];
}

class LoginErro extends LoginState {
  const LoginErro({this.mensageError});

  final String mensageError;

  @override
  List<Object> get props => [];
}

class LoginInternetOff extends LoginState {
  const LoginInternetOff({this.mensageError});

  final String mensageError;

  @override
  List<Object> get props => [];
}

class LoginSucesso extends LoginState {
  @override
  List<Object> get props => [];
}
