import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:teste_ioasys_app/app/common/headers/domain/repositories/iheaders_repository.dart';
import 'package:teste_ioasys_app/app/core/network/api_result.dart';
import 'package:teste_ioasys_app/app/features/login/domain/usecases/submeter_login_usecase.dart';
import 'package:teste_ioasys_app/injection_container.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit({
    SubmeterLoginUsecase submeterLoginUsecase,
    IHeadersRepository iHeadersRepository,
  })  : _submeterLoginUsecase =
            submeterLoginUsecase ?? dependency<SubmeterLoginUsecase>(),
        _iHeadersRepository =
            iHeadersRepository ?? dependency<IHeadersRepository>(),
        super(LoginInitial());

  SubmeterLoginUsecase _submeterLoginUsecase;
  IHeadersRepository _iHeadersRepository;

  void submeterLogin(String email, String password) async {
    emit(LoginLoading());

    final resultado = await _submeterLoginUsecase(
      email: email,
      password: password,
    );

    resultado.fold(
      (erro) {
        if (erro is Erro) {
          emit(LoginErro(mensageError: erro.message));
        } else {
          emit(LoginInternetOff(mensageError: erro.message));
        }
      },
      (resposta) {
        _iHeadersRepository.recordHeaders(
          uid: resposta.value('uid'),
          client: resposta.value('client'),
          accessToken: resposta.value('access-token'),
        );
        emit(LoginSucesso());
      },
    );
  }
}
