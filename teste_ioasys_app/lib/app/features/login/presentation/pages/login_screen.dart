import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teste_ioasys_app/app/common/coordinator/main_coordinator.dart';
import 'package:teste_ioasys_app/app/features/login/presentation/components/cabecalho_circular_widget.dart';
import 'package:teste_ioasys_app/app/features/login/presentation/components/campos_entrada_login_widget.dart';
import 'package:teste_ioasys_app/app/common/ui/carregamento_widget.dart';
import 'package:teste_ioasys_app/app/features/login/presentation/cubit/login_cubit.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<LoginCubit>(
        create: (context) => LoginCubit(),
        child: BlocConsumer<LoginCubit, LoginState>(
          listener: (context, state) {
            if (state is LoginSucesso) {
              MainCoordinator.navigateToHome();
            }
          },
          builder: (context, state) {
            return Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      CabecalhoCircularWidget(),
                      CamposEntradaLoginWidget(
                        isCredenciaisValidas: !(state is LoginErro),
                        mensageError: _mensageErrorForm(state),
                      ),
                    ],
                  ),
                ),
                if (state is LoginLoading)
                  Opacity(
                    opacity: 0.8,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.black,
                      ),
                    ),
                  ),
                if (state is LoginLoading)
                  Center(
                    child: LoadingWidget(),
                  )
              ],
            );
          },
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  String _mensageErrorForm(LoginState state) {
    if (state is LoginErro) {
      return state.mensageError;
    } else {
      return '';
    }
  }
}
