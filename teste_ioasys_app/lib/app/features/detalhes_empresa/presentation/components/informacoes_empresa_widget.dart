import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:teste_ioasys_app/app/common/ui/cores.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';

class InfoCompanyWidget extends StatelessWidget {
  const InfoCompanyWidget({@required this.company});

  final Company company;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 36.0),
          child: Text(
            company.description,
            style: GoogleFonts.rubik(
              fontSize: 18,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Icon(
                MdiIcons.briefcase,
                color: Cores.darkGray,
              ),
            ),
            Text(
              '${company.enterpriseType.enterpriseTypeName}',
              style: GoogleFonts.rubik(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            )
          ],
        ),
        SizedBox(height: 8.0),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Icon(
                MdiIcons.city,
                color: Cores.darkGray,
              ),
            ),
            Text(
              '${company.city}, ${company.country}',
              style: GoogleFonts.rubik(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            )
          ],
        ),
        SizedBox(height: 8.0),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Icon(
                MdiIcons.currencyUsd,
                color: Cores.darkGray,
              ),
            ),
            Text(
              '${company.value}',
              style: GoogleFonts.rubik(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            )
          ],
        ),
        SizedBox(height: 8.0),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Icon(
                MdiIcons.accountCash,
                color: Cores.darkGray,
              ),
            ),
            Text(
              '${company.sharePrice}',
              style: GoogleFonts.rubik(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            )
          ],
        ),
        SizedBox(height: 8.0),
        if (company.emailEnterprise != null)
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Icon(
                  MdiIcons.email,
                  color: Cores.darkGray,
                ),
              ),
              Text(
                company.emailEnterprise ?? '',
                style: GoogleFonts.rubik(
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                ),
              )
            ],
          ),
        SizedBox(height: 8.0),
        if (company.twitter != null)
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Icon(
                  MdiIcons.twitter,
                  color: Cores.darkGray,
                ),
              ),
              Text(
                company.twitter ?? '',
                style: GoogleFonts.rubik(
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                ),
              )
            ],
          ),
        SizedBox(height: 8.0),
        if (company.linkedin != null)
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Icon(
                  MdiIcons.linkedin,
                  color: Cores.darkGray,
                ),
              ),
              Text(
                company.linkedin ?? '',
                style: GoogleFonts.rubik(
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                ),
              )
            ],
          ),
        SizedBox(height: 8.0),
        if (company.phone != null)
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Icon(
                  MdiIcons.phone,
                  color: Cores.darkGray,
                ),
              ),
              Text(
                company.phone ?? '',
                style: GoogleFonts.rubik(
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                ),
              )
            ],
          ),
      ],
    );
  }
}
