import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:teste_ioasys_app/app/features/detalhes_empresa/presentation/components/informacoes_empresa_widget.dart';
import 'package:teste_ioasys_app/app/common/ui/cores.dart';
import 'package:teste_ioasys_app/app/common/ui/empresa_titulo_imagem_widget.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';

class DetailsCompanyScreen extends StatelessWidget {
  const DetailsCompanyScreen({@required this.company});

  final Company company;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
              color: Cores.whiteSmoke,
              borderRadius: new BorderRadius.all(
                Radius.circular(4),
              ),
            ),
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back_sharp,
                color: Cores.ruby,
              ),
            ),
          ),
        ),
        title: Text(
          company.enterpriseName,
          style: GoogleFonts.rubik(
            fontSize: 20,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            EmpresaTituloImagemWidget2(
              company: company,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 24.0,
                left: 16.0,
                right: 16.0,
              ),
              child: InfoCompanyWidget(
                company: company,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
