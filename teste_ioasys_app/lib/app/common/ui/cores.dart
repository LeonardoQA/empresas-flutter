import 'dart:ui';

abstract class Cores {
  static const Color darkGray = Color(0xFF666666);
  static const Color ruby = Color(0xFFE01E69);
  static const Color gainsboro = Color(0xFFDBDBDB);
  static const Color darkOrchid = Color(0xFF8327BA);
  static const Color shocking = Color(0xFFD995BC);
  static const Color seagull = Color(0xFF79BBCA);
  static const Color sweetRose = Color(0xFFEB9797);
  static const Color darkNavyGreen = Color(0xFF90BB81);
  static const Color tahunaSands = Color(0xFFD9D295);
  static const Color bilobaFlower = Color(0xFFA595D9);
  static const Color whiteSmoke = Color(0xFFF5F5F5);

  static const List<Color> _coresFundoItemLista = [
    ruby,
    bilobaFlower,
    tahunaSands,
    darkOrchid,
    seagull,
    shocking,
    sweetRose,
    darkNavyGreen,
  ];

  static Color getColorBackgroundItemList(int index) {
    return _coresFundoItemLista[index % _coresFundoItemLista.length];
  }
}
