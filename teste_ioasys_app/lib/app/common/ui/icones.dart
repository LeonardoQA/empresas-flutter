import 'package:meta/meta.dart';

abstract class Icones {
  static String _common({@required String nameIcon}) {
    return 'assets/images/common/$nameIcon.png';
  }

  static String _login({@required String nameIcon}) {
    return 'assets/images/login/$nameIcon.png';
  }

  static String _splash({@required String nameIcon}) {
    return 'assets/images/splash/$nameIcon.png';
  }

  static String _home({@required String nameIcon}) {
    return 'assets/images/home/$nameIcon.png';
  }

  static String get logoIcon => _login(nameIcon: 'logo');
  static String get iconLogoWithTitle => _splash(nameIcon: 'logo_com_titulo');
  static String get background => _common(nameIcon: 'background');
  static String get iconError => _common(nameIcon: 'icone_erro');
  static String get greaterLoadingArc =>
      _login(nameIcon: 'arco_carregamento_maior');
  static String get smallerLoadingArc =>
      _login(nameIcon: 'arco_carregamento_menor');
  static String get logoIconLowOpacity1 =>
      _home(nameIcon: 'icone_logo_apagado_1');
  static String get logoIconLowOpacity2 =>
      _home(nameIcon: 'icone_logo_apagado_2');
  static String get logoIconLowOpacity3 =>
      _home(nameIcon: 'icone_logo_apagado_3');
  static String get logoIconLowOpacity4 =>
      _home(nameIcon: 'icone_logo_apagado_4');
}
