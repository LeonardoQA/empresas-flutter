abstract class Strings {
  static const String welcomeCompanies = 'Seja bem vindo ao empresas!';
  static const String email = 'Email';
  static const String password = 'Senha';
  static const String enter = 'Entrar';
  static const String incorrectCredentials = 'Credenciais incorretas';
  static const String algoInesperadoAconteceu =
      'Algo inesperado aconteceu, tente novamente';
  static const String withoutInternet =
      'Não há conexão com a internet, tente novamente mais tarde';
  static const String searchByCompany = 'Pesquise por empresa';
  static const String noResult = 'Nenhum resultado encontrado';

  static String resultsFound(int amount) {
    final stringAmount = amount.toString().padLeft(2, '0');

    if (amount > 1) {
      return '$stringAmount resultados encontrados';
    } else {
      return '$stringAmount resultado encontrado';
    }
  }

  static String urlBaseComEndpoint(String endpoint) {
    return 'https://empresas.ioasys.com.br/$endpoint';
  }
}
