import 'package:flutter/material.dart';

abstract class SizingUtils {
  static double heightHeaderCircle(BuildContext context, bool isTecladoAberto) {
    return MediaQuery.of(context).size.height * (isTecladoAberto ? 0.2 : 0.35);
  }

  static double heightHeaderRectangle(
      BuildContext context, bool isTecladoAberto) {
    return MediaQuery.of(context).size.height *
        (isTecladoAberto ? 0.075 : 0.25);
  }

  static double heightTextHeaderRectangle(
      BuildContext context, bool isTecladoAberto) {
    return heightHeaderRectangle(context, isTecladoAberto) - 24;
  }
}
