import 'package:flutter/material.dart';
import 'package:teste_ioasys_app/app/common/routes/routes.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';

abstract class MainCoordinator {
  static final _navigationKey = GlobalKey<NavigatorState>();

  static GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  static void navigateToLogin() {
    _navigationKey.currentState
        .pushNamedAndRemoveUntil(Routes.loginScreen, (route) => false);
  }

  static void navigateToHome() {
    _navigationKey.currentState
        .pushNamedAndRemoveUntil(Routes.homeScreen, (route) => false);
  }

  static void navigateToCompanyDetails(Company company) {
    _navigationKey.currentState.pushNamed(
      Routes.detailsCompanyScreen,
      arguments: company,
    );
  }
}
