import 'package:get_it/get_it.dart';
import 'package:teste_ioasys_app/app/common/headers/domain/usecases/gravar_headers_usecase.dart';
import 'package:teste_ioasys_app/app/common/headers/domain/usecases/ler_headers_usecase.dart';
import 'package:teste_ioasys_app/app/features/home/data/datasources/empresas_datasource.dart';
import 'package:teste_ioasys_app/app/features/home/domain/repositories/i_empresas_repository.dart';
import 'package:teste_ioasys_app/app/features/home/domain/usecases/consultar_empresas_usecase.dart';
import 'package:teste_ioasys_app/app/features/login/data/datasources/login_datasource.dart';
import 'package:teste_ioasys_app/app/features/login/domain/usecases/submeter_login_usecase.dart';
import 'package:teste_ioasys_app/app/features/login/data/repositories/login_repository.dart';
import 'package:teste_ioasys_app/app/features/login/domain/repositories/i_login_repository.dart';
import 'app/common/headers/data/datasources/headers_datasource.dart';
import 'app/common/headers/data/repositories/headers_repository.dart';
import 'app/common/headers/domain/repositories/iheaders_repository.dart';
import 'app/features/home/data/repositories/empresas_repository.dart';

final dependency = GetIt.instance;

Future<void> init() async {
  dependency.registerLazySingleton<LoginDatasource>(
    () => LoginDatasource(),
  );

  dependency.registerLazySingleton<ILoginRepository>(
    () => LoginRepository(
      loginDatasource: dependency(),
    ),
  );

  dependency.registerLazySingleton<submitLoginUsecase>(
    () => submitLoginUsecase(
      repository: dependency(),
    ),
  );

  dependency.registerLazySingleton<HeadersDatasource>(
    () => HeadersDatasource(),
  );

  dependency.registerLazySingleton<IHeadersRepository>(
    () => HeadersRepository(
      headersDatasource: dependency(),
    ),
  );

  dependency.registerLazySingleton<GravarHeadersUsecase>(
    () => GravarHeadersUsecase(
      repository: dependency(),
    ),
  );

  dependency.registerLazySingleton<LerHeadersUsecase>(
    () => LerHeadersUsecase(
      repository: dependency(),
    ),
  );

  dependency.registerLazySingleton<CompaniesDatasource>(
    () => CompaniesDatasource(),
  );

  dependency.registerLazySingleton<IEmpresasRepository>(
    () => CompaniesRepository(
      companiesDatasource: dependency(),
    ),
  );

  dependency.registerLazySingleton<ConsultarEmpresasUsecase>(
    () => ConsultarEmpresasUsecase(
      repository: dependency(),
    ),
  );
}
