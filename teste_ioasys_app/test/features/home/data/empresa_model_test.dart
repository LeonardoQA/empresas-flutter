import 'package:flutter_test/flutter_test.dart';
import 'package:teste_ioasys_app/app/features/home/data/models/enterprise_type_model.dart';
import 'package:teste_ioasys_app/app/features/home/data/models/empresa_model.dart';
import 'package:teste_ioasys_app/app/features/home/domain/entities/company.dart';

void main() {
  EnterpriseTypeModel tEnterpriseType;
  CompanyModel tEmpresa;

  setUp(() {
    tEnterpriseType = EnterpriseTypeModel(
      enterpriseTypeName: 'textile',
      id: 6,
    );

    tEmpresa = CompanyModel(
        id: 423,
        city: 'aracaju',
        country: 'BR',
        description: 'uma company',
        emailEnterprise: '',
        enterpriseName: 'Pizza do bairro',
        enterpriseType: tEnterpriseType,
        facebook: '',
        twitter: '',
        linkedin: '',
        phone: '',
        value: 0,
        sharePrice: 0.0,
        ownEnterprise: false,
        photo: '');
  });

  test('CompanyModel deve ser subtipo de Company', () async {
    expect(tEmpresa, isA<Company>());
  });
}
